<?php

include_once "Trajet.php";
include_once "Model.php";

    $tab=[];
    $tab=Trajet::getPassagers($_GET['identifiant']);
    if($tab!=null){

        echo "<p> Les passagers du trajet numéro {$_GET['identifiant']} sont les suivants:</p>";
        foreach ($tab as $utilisateur){

            $utilisateur->afficher();
        }
    } else{
        echo "<p> Erreur: ce trajet n'existe pas ou il n'y a aucun passager </p>";
    }

?>