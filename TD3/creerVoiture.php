<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title> Votre voiture</title>
</head>
<body>

    <?php
    include_once "Voiture.php";

        echo "<p> Votre voiture {$_POST['marque']}, d'immatriculation {$_POST['immatriculation']}, de couleur {$_POST['couleur']} et avec {$_POST['nbSieges']} sièges, a été ajoutée à la base de donnée.  </p>";
        $newVoiture=new Voiture($_POST['marque'],$_POST['couleur'],$_POST['immatriculation'],$_POST['nbSieges']);
        $newVoiture->sauvegarder();
    ?>
</body>
</html>