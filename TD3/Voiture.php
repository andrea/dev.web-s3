<?php
include_once "Model.php";
class Voiture{

    private string $marque;
    private string $couleur;
    private string $immatriculation;
    private int $nbSieges;

    public function getCouleur() : string
    {
        return $this->couleur;
    }


    public function setCouleur(string $couleur)
    {
        $this->couleur = $couleur;
    }


    public function getImmatriculation() : string
    {
        return $this->immatriculation;
    }


    public function setImmatriculation(string $immatriculation)
    {
        $rest= substr($immatriculation,0, 8);
        $this->immatriculation = $rest;
    }


    public function getNbSieges() : int
    {
        return $this->nbSieges;
    }

    public function setNbSieges(int $nbSieges)
    {
        $this->nbSieges = $nbSieges;
    }


    public function getMarque() : string
    {
        return $this->marque;
    }

    public function setMarque(string $marque){
        $this->marque =$marque;
    }

    public function __construct($marque,$couleur,$immatriculation,$nbSieges){
        $this->marque=$marque;
        $this->couleur=$couleur;
        $this->immatriculation=substr($immatriculation,0,8);
        $this->nbSieges=$nbSieges;
    }



    public function afficher(){
        echo "<p> Informations : <lu><li>
                    $this->marque</li>
                    <li>$this->couleur</li>
                    <li>$this->immatriculation</li>
                    <li>$this->nbSieges</li></lu> </p>";
    }

    public static function construire (array $voitureFormatTableau) : Voiture {
        $voiture1= new static($voitureFormatTableau['marqueBDD'],$voitureFormatTableau['couleurBDD'],$voitureFormatTableau['immatriculationBDD'],$voitureFormatTableau['nbSiegesBDD']);
        return $voiture1;
    }

    public static function getVoitures() : array
    {
        $tabVoiture=[];
        $pdo= Model::getPdo();
        $pdoStatement= $pdo->query("SELECT* FROM voiture");
        foreach($pdoStatement as $voitureFormatTableau){

            $voiture1=self::construire($voitureFormatTableau);
            $tabVoiture[]=$voiture1;
        }
        return $tabVoiture;

    }

    public static function getVoitureParImmat(string $immatriculation) : ?Voiture {
        $sql = "SELECT * from voiture WHERE immatriculationBDD=:immatriculationTag";
        // Préparation de la requête
        $pdoStatement = Model::getPdo()->prepare($sql);

        $values = array(
            "immatriculationTag" => $immatriculation,
            //nomdutag => valeur, ...
        );
        // On donne les valeurs et on exécute la requête
        $pdoStatement->execute($values);


        // On récupère les résultats comme précédemment
        // Note: fetch() renvoie false si pas de voiture correspondante
        $voiture = $pdoStatement->fetch();

        if($voiture){
        return static::construire($voiture);
        } else{
            return null;
        }


    }

    public function sauvegarder() : void{
        $sql="INSERT INTO voiture VALUES (:immatriculationTag,:marqueTag,:couleurTag,:nbSiegesTag)";

        $pdoStatement= Model::getPdo()->prepare($sql);
        $values=array(
            "immatriculationTag"=>$this->immatriculation,
            "marqueTag"=>$this->marque,
            "couleurTag"=>$this->couleur,
            "nbSiegesTag"=>$this->nbSieges
        );
        $pdoStatement->execute($values);

    }
}

?>
