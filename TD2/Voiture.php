<?php
class Voiture{
    private string $marque;
    private string $couleur;
    private string $immatriculation;
    private int $nbSieges;

    public function getCouleur() : string
    {
        return $this->couleur;
    }


    public function setCouleur(string $couleur)
    {
        $this->couleur = $couleur;
    }


    public function getImmatriculation() : string
    {
        return $this->immatriculation;
    }


    public function setImmatriculation(string $immatriculation)
    {
        $rest= substr($immatriculation,0, 8);
        $this->immatriculation = $rest;
    }


    public function getNbSieges() : int
    {
        return $this->nbSieges;
    }

    public function setNbSieges(int $nbSieges)
    {
        $this->nbSieges = $nbSieges;
    }


    public function getMarque() : string
    {
        return $this->marque;
    }

    public function setMarque(string $marque){
        $this->marque =$marque;
    }

    public function __construct($marque,$couleur,$immatriculation,$nbSieges){
        $this->marque=$marque;
        $this->couleur=$couleur;
        $this->immatriculation=substr($immatriculation,0,8);
        $this->nbSieges=$nbSieges;
    }



    public function afficher(){
        echo "<p> Informations : <lu><li>
                    $this->marque</li>
                    <li>$this->couleur</li>
                    <li>$this->immatriculation</li>
                    <li>$this->nbSieges</li></lu> </p>";
    }

    public static function builder (array $voitureFormatTableau) : Voiture {
        $voiture1= new static($voitureFormatTableau['marqueBDD'],$voitureFormatTableau['couleurBDD'],$voitureFormatTableau['immatriculationBDD'],$voitureFormatTableau['nbSiegesBDD']);
        return $voiture1;
    }

    public static function getVoitures() : array
    {
        $tabVoiture=[];
        $pdo= Model::getPdo();
        $pdoStatement= $pdo->query("SELECT* FROM voiture");

        foreach($pdoStatement as $voitureFormatTableau){

            $voiture1=self::builder($voitureFormatTableau);
            $voiture1->afficher();
            $tabVoiture[]=$voiture1;
        }
        return $tabVoiture;

    }


}

?>
