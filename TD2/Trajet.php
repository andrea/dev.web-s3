<?php

class Trajet{
    private string $id;
    private string $depart;
    private string $arrivee;
    private string $date;
    private int $nbPlace;
    private float $prix;
    private string $conducteurLogin;

    public function afficher(){
        echo "<div><p>Informations du trajet </p><p>identifiant: $this->id</p><p>depart: $this->depart</p><p>arrivée: $this->arrivee</p><p>date: $this->date</p><p>nombre de Place: $this->nbPlace
                </p><p>prix: $this->prix</p><p>login du conducteur: $this->conducteurLogin</p></div>";
    }
    public function __construct($id,$depart,$arrivee,$date,$nbPlace,$prix,$conducteurLogin)
    {
        $this->id=$id;
        $this->depart=$depart;
        $this->arrivee=$arrivee;
        $this->date=$date;
        $this->nbPlace=$nbPlace;
        $this->prix=$prix;
        $this->conducteurLogin=$conducteurLogin;
    }

    public static function builder($trajetFormatTab) : Trajet {
        $trajet=new static($trajetFormatTab['id'],$trajetFormatTab['depart'],$trajetFormatTab['arrivee'],$trajetFormatTab['date'],$trajetFormatTab['nbPlaces'],$trajetFormatTab['prix'],$trajetFormatTab['conducteurLogin']);
        return $trajet;
    }

    public static function getTrajet() : array {

        $tabTrajet=[];
        $pdo=Model::getPdo();
        $pdoStatement=$pdo->query("SELECT* FROM trajet ");

        foreach ($pdoStatement as $trajetFormatTableau){
            $trajet1=self::builder($trajetFormatTableau);
            $tabTrajet=[$trajet1];
            $trajet1->afficher();
        }
        return $tabTrajet;

    }

}
?>