<?php

namespace App\Covoiturage\Model\DataObject;

    class Utilisateur{
        private string $login;
        private string $nom;


        public function getLogin(): string
        {
            return $this->login;
        }


        public function setLogin(string $login): void
        {
            $this->login = $login;
        }


        public function getNom(): string
        {
            return $this->nom;
        }


        public function setNom(string $nom): void
        {
            $this->nom = $nom;
        }


        public function getPrenom(): string
        {
            return $this->prenom;
        }


        public function setPrenom(string $prenom): void
        {
            $this->prenom = $prenom;
        }
        private string $prenom;

        public function __construct($login,$nom,$prenom){
            $this->prenom=$prenom;
            $this->nom=$nom;
            $this->login=$login;
        }

    }

    ?>