<?php

namespace App\Covoiturage\Model\Repository;
//use App\Covoiturage\Model\Repository\DatabaseConnection;
use App\Covoiturage\Model\DataObject\Voiture;

class VoitureRepository
{


    public static function getVoitures(): array
    {
        $tabVoiture = [];
        $pdo = DatabaseConnection::getPdo();
        $pdoStatement = $pdo->query("SELECT* FROM voiture");
        foreach ($pdoStatement as $voitureFormatTableau) {

            //$voiture1 = Voiture::construire($voitureFormatTableau);
            $voiture1=self::construire($voitureFormatTableau);

            $tabVoiture[] = $voiture1;
        }

        return $tabVoiture;

    }

    public static function getVoitureParImmat(string $immatriculation): ?Voiture
    {
        $sql = "SELECT * from voiture WHERE immatriculationBDD=:immatriculationTag";
        // Préparation de la requête
        $pdoStatement = DatabaseConnection::getPdo()->prepare($sql);

        $values = array(
            "immatriculationTag" => $immatriculation,
            //nomdutag => valeur, ...
        );
        // On donne les valeurs et on exécute la requête
        $pdoStatement->execute($values);


        // On récupère les résultats comme précédemment
        // Note: fetch() renvoie false si pas de voiture correspondante
        $voiture = $pdoStatement->fetch();

        if ($voiture) {
            return static::construire($voiture);
        } else {
            return null;
        }
    }

    public static function sauvegarder(Voiture $voit): void
    {
        $sql = "INSERT INTO voiture VALUES (:immatriculationTag,:marqueTag,:couleurTag,:nbSiegesTag)";

        $pdoStatement = DatabaseConnection::getPdo()->prepare($sql);
        $values = array(
            "immatriculationTag" => $voit->getImmatriculation(),
            "marqueTag" => $voit->getMarque(),
            "couleurTag" => $voit->getCouleur(),
            "nbSiegesTag" => $voit->getNbSieges()
        );
        $pdoStatement->execute($values);

    }

    public static function construire(array $voitureFormatTableau): Voiture
    {
        $voiture1 = new Voiture($voitureFormatTableau['marqueBDD'], $voitureFormatTableau['couleurBDD'], $voitureFormatTableau['immatriculationBDD'], $voitureFormatTableau['nbSiegesBDD']);
        return $voiture1;
    }

    public static function supprimerParImmatriculation(int $imma){
        $sql="DELETE FROM voiture WHERE immatriculationBDD=:immatriculationTag";
        $values= array(
            "immatriculationTag"=>$imma
        );
        $pdoStatement = DatabaseConnection::getPdo()->prepare($sql);
        $pdoStatement->execute($values);

    }

    public static function mettreAJour(Voiture $voiture): void {
        $sql="UPDATE voiture SET marqueBDD=:marqueTag, couleurBDD=:couleurTag,nbSiegesBDD=:nbSiegesTag WHERE immatriculationBDD=:immaTag";
        $values=array(
            "marqueTag"=>$voiture->getMarque(),
            "couleurTag"=>$voiture->getCouleur(),
            "nbSiegesTag"=>$voiture->getNbSieges(),
            "immaTag"=> $voiture->getImmatriculation()
        );
        $pdoStatement = DatabaseConnection::getPdo()->prepare($sql);
        $pdoStatement->execute($values);

    }
}
?>