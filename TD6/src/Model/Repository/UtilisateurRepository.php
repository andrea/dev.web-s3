<?php

    namespace App\Covoiturage\Model\Repository;
    use App\Covoiturage\Model\DataObject\Utilisateur as Utilisateur;

class UtilisateurRepository{

        public static function getUtilisateurs(): array {
            $tabUtilisateurs=[];

            $pdo = DatabaseConnection::getPdo();
            $pdoStatement = $pdo->query("SELECT* FROM utilisateur");
            foreach ($pdoStatement as $utilisateurFormatTab){
                $utilisateur=self::construire($utilisateurFormatTab);
                $tabUtilisateurs[]=$utilisateur;
            }
            return $tabUtilisateurs;
        }

        public static function construire(array $utilisateurFormatTableau): Utilisateur
        {
            $utilisateur1 = new Utilisateur($utilisateurFormatTableau['login'], $utilisateurFormatTableau['nom'], $utilisateurFormatTableau['prenom']);
            return $utilisateur1;
        }

    }
?>>