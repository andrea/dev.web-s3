<?php
//require_once __DIR__ . '/../DatabaseConnection/Voiture.php';
namespace App\Covoiturage\Controller;
use App\Covoiturage\Model\Repository\VoitureRepository;
use App\Covoiturage\Model\DataObject\Voiture;



class ControllerVoiture {
    // Déclaration de type de retour void : la fonction ne retourne pas de valeur
    public static function readAll() : void {
        $voitures = VoitureRepository::getVoitures(); //appel au modèle pour gerer la BD

        //require ('../view/voiture/list.php');  //"redirige" vers la vue
        ControllerVoiture::afficheVue("view.php", ["voitures" =>$voitures,"pagetitle" => "Liste des voitures SHESH", "cheminVueBody" => "voiture/list.php"]);
    }

    public static function read(int $imma) : void {
    $voiture = VoitureRepository::getVoitureParImmat($imma);
    if($voiture!=null) {
      //  require('../view/voiture/detail.php');
        ControllerVoiture::afficheVue("view.php",["voiture"=>$voiture,"pagetitle" => "Detail de la voiture " . $voiture->getImmatriculation(), "cheminVueBody" => "voiture/detail.php"]);
    }else{
       // require ('../view/voiture/error.php');
        $errorMessage=" La voiture n'existe pas. ";

        ControllerVoiture::afficheVue("view.php",["pagetitle" => "ERROOOR", "cheminVueBody" => "voiture/error.php", "errorMessage"=>$errorMessage]);
    }
    }

    public static function create(): void {
        ControllerVoiture::afficheVue("view.php",["pagetitle" => "Création", "cheminVueBody" => "voiture/create.php"]);
    }

    public static function created() :void {
        $marque=htmlspecialchars($_GET['marque']);
        $couleur=htmlspecialchars($_GET['couleur']);
        $immatriculation=htmlspecialchars($_GET['immatriculation']);
        $nbSieges=htmlspecialchars($_GET['nbSieges']);
        $voiture= new Voiture($marque,$couleur,$immatriculation,$nbSieges);
       // $voiture->sauvegarder();
        VoitureRepository::sauvegarder($voiture);
        $voitures = VoitureRepository::getVoitures();
        ControllerVoiture::afficheVue("view.php",["pagetitle"=> "voiture crée","cheminVueBody"=>"voiture/created.php","voitures"=>$voitures]);
    }

    public static function error(string $errorMessage){

        ControllerVoiture::afficheVue("view.php",["pagetitle"=>"giga error", "cheminVueBody"=>"voiture/error.php","errorMessage"=>$errorMessage]);
    }

    private static function afficheVue(string $cheminVue, array $parametres = []) : void {
        extract($parametres); // Crée des variables à partir du tableau $parametres
        // require "../view/$cheminVue"; // Charge la vue
        require_once __DIR__ . "/../view/$cheminVue";
    }

    public static function delete (int $imma):void {
        VoitureRepository::supprimerParImmatriculation($imma);
        $voitures = VoitureRepository::getVoitures();
        ControllerVoiture::afficheVue("view.php",["pagetitle" => "Supprimation", "cheminVueBody" => "voiture/deleted.php","voitures"=>$voitures]);
    }

    public static function update(){
        ControllerVoiture::afficheVue("view.php",["pagetitle" => "Modification", "cheminVueBody" => "voiture/update.php"]);
    }

    public static function updated(int $imma){

        $marque=htmlspecialchars($_GET['marque']);
        $couleur=htmlspecialchars($_GET['couleur']);
        $nbSieges=htmlspecialchars($_GET['nbSieges']);
        $voiture= new Voiture($marque,$couleur,$imma,$nbSieges);
        VoitureRepository::mettreAJour($voiture);
        $voitures=VoitureRepository::getVoitures();
        ControllerVoiture::afficheVue("view.php",["pagetitle"=> "voiture crée","cheminVueBody"=>"voiture/updated.php","voitures"=>$voitures]);

    }

    


}
?>