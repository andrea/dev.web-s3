<?php

namespace App\Covoiturage\Controller;

use App\Covoiturage\Model\Repository\UtilisateurRepository;

    class ControllerUtilisateur
    {

        public static function readAll(): void
        {
            $utilisateurs = UtilisateurRepository::getUtilisateurs(); //appel au modèle pour gerer la BD

            //require ('../view/voiture/list.php');  //"redirige" vers la vue
            ControllerUtilisateur::afficheVue("view.php", ["utilisateurs" => $utilisateurs, "pagetitle" => "Liste des utilisateurs SHESH", "cheminVueBody" => "utilisateur/list.php"]);
        }

        private static function afficheVue(string $cheminVue, array $parametres = []): void
        {
            extract($parametres); // Crée des variables à partir du tableau $parametres
            // require "../view/$cheminVue"; // Charge la vue
            require_once __DIR__ . "/../view/$cheminVue";
        }

        public static function error(string $errorMessage)
        {

            ControllerUtilisateur::afficheVue("view.php", ["pagetitle" => "giga error", "cheminVueBody" => "voiture/error.php", "errorMessage" => $errorMessage]);
        }
    }
?>