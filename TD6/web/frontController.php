<?php
//require_once 'ControllerVoiture.php';
require_once __DIR__. '/../src/Lib/Psr4AutoloaderClass.php';
use App\Covoiturage\Controller\ControllerVoiture;

// require_once __DIR__ . '/../src/Controller/ControllerVoiture.php';



// On recupère l'action passée dans l'URL
// URL examples:
//https://webinfo.iutmontp.univ-montp2.fr/~andrea/TD6/web/frontController.php?action=readAll
//https://webinfo.iutmontp.univ-montp2.fr/~andrea/TD6/web/frontController.php?controller=voiture&action=readAll

    // instantiate the loader
    $loader = new App\Covoiturage\Lib\Psr4AutoloaderClass();
    // register the base directories for the namespace prefix
    $loader->addNamespace('App\Covoiturage', __DIR__ . '/../src');
    // register the autoloader
    $loader->register();

    $controller="voiture";
    if(isset($_GET['controller'])){
        $controller=$_GET['controller'];
    }
    $controllerClassName='App\Covoiturage\Controller\Controller'.ucfirst($controller);



    $action="readAll";
    if(isset($_GET['action'])){
        $action =$_GET['action'];
    }

    $commande =get_class_methods("App\Covoiturage\Controller\ControllerVoiture");


// Appel de la méthode statique $action de ControllerVoiture

    if(class_exists($controllerClassName)) {
        if (in_array($action, $commande)) {
            switch ($action) {
                case "readAll":
                    $controllerClassName::$action();
                    break;
                case "delete":
                    $controllerClassName::$action($_GET["immatriculation"]);
                    break;
                case "read":
                    $controllerClassName::$action($_GET["immatriculation"]);
                    break;
                case "created":
                    $controllerClassName::$action();
                    break;
                case "create":
                    $controllerClassName::$action();
                    break;
                case "update":
                    $controllerClassName::$action();
                    break;
                case "updated":
                    $controllerClassName::$action($_GET["immatriculation"]);

            }
        } else {
            ControllerVoiture::error(" Action inexistante. ");
        }
    }else {
        ControllerVoiture::error(" Le controller n'existe pas.");
    }

?>