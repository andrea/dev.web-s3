<?php
class Voiture{
    private string $marque;
    private string $couleur;
    private string $immatriculation;
    private int $nbSieges;

    public function getCouleur() : string
    {
        return $this->couleur;
    }


    public function setCouleur(string $couleur)
    {
        $this->couleur = $couleur;
    }


    public function getImmatriculation() : string
    {
        return $this->immatriculation;
    }


    public function setImmatriculation(string $immatriculation)
    {
        $rest= substr($immatriculation,0, 8);
        $this->immatriculation = $rest;
    }


    public function getNbSieges() : int
    {
        return $this->nbSieges;
    }

    public function setNbSieges(int $nbSieges)
    {
        $this->nbSieges = $nbSieges;
    }


    public function getMarque() : string
    {
        return $this->marque;
    }

    public function setMarque(string $marque){
        $this->marque =$marque;
    }

    public function __construct($marque,$couleur,$immatriculation,$nbSieges){
        $this->marque=$marque;
        $this->couleur=$couleur;
        $this->immatriculation=substr($immatriculation,0,8);
        $this->nbSieges=$nbSieges;
    }



    public function afficher(){
        echo "<p> Informations : <lu><li>
                    $this->marque</li>
                    <li>$this->couleur</li>
                    <li>$this->immatriculation</li>
                    <li>$this->nbSieges</li></lu> </p>";
    }

}
        $v1= new Voiture("Renault","bleu","847462867899",5);
        $v1->afficher();
?>
