<!DOCTYPE html>
<html>
<head>
    <title>Exo 4</title>
    <meta charset="utf-8">
</head>
<body>
    php :
    <?php
    $prenom="Marc";
    echo 'Bonjour '.$prenom;
    echo " Bonjour $prenom <br> ";
    echo $prenom;
    echo " $prenom";
    print_r($prenom);
    echo "<br>";
    var_dump($prenom);
// prenom est une "cle" ou "index", et Juste est la "valeur"
    $utilisateur = array(
        'prenom' => 'Juste',
        'nom' => 'Leblanc');

    $utilisateur2 = [
            'prenom'=>'Martin',
        'nom'=> 'Baka'];

    $utilisateur['passion']='genshin';

    foreach ($utilisateur as $lindex => $lavaleur){
        echo "<p> $lindex vaut $lavaleur </p>";
    }
    var_dump($utilisateur2);
    ?>
</body>
</html>