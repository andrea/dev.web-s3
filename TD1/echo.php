<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title> Mon premier php </title>
</head>

<body>
Voici le résultat du script PHP :
<?php
 // Ceci est un commentaire PHP sur une ligne
 /* Ceci est le 2ème type de commentaire PHP
 sur plusieurs lignes */

 // On met la chaine de caractères "hello" dans la variable 'texte'
 // Les noms de variable commencent par $ en PHP

$texte = "hello world uwu !<br>";
 // On écrit le contenu de la variable 'texte' dans la page Web
 echo $texte;

 $marque="renault";
 $couleur="rouge";
 $immatriculation="8390";
 $nbsieges=5;
 echo "<p>Voiture $immatriculation de marque $marque (couleur $couleur, $nbsieges sièges.)</p>";
 $voiture1 =[
         "marque"=> "Nissan",
        "couleur"=> "bleu",
        "immatriculation"=> "7839",
        "nbSieges"=> 6
 ];
$voiture2 =[
    "marque"=> "Nissan",
    "couleur"=> "noir",
    "immatriculation"=> "9376",
    "nbSieges"=> 3
];
print_r($voiture1);
echo "<br>";
var_dump($voiture1);
echo "<p> La voiture $voiture1[immatriculation] de marque $voiture1[marque] (couleur $voiture1[couleur], $voiture1[nbSieges] sièges). </p>";

$voitures=[
        0=>$voiture1,
    1=> $voiture2,
];

print_r($voitures);
echo "<p> </p>";
var_dump($voitures);

foreach ($voitures as $cle => $valeur){
    echo "<p> La voiture $valeur[immatriculation] de marque $valeur[marque] (couleur $valeur[couleur], $valeur[nbSieges] sièges).  </p>";
}


foreach($voitures as $cle=> $valeur){
    echo "<p><ul>
            <li>immatriculation= $valeur[immatriculation]</li>
            <li>marque = $valeur[marque]</li>
            <li>couleur= $valeur[couleur]</li>
            <li>nbSieges = $valeur[nbSieges]</li>
</ul></p>";
}
if(empty($voitures)){
    echo "Il n'y a aucune voiture";
}

?>
</body>
</html>