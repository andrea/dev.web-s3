<?php
//require_once __DIR__ . '/../Model/ModelVoiture.php';
namespace App\Covoiturage\Controller;
use App\Covoiturage\Model\ModelVoiture;



class ControllerVoiture {
    // Déclaration de type de retour void : la fonction ne retourne pas de valeur
    public static function readAll() : void {
        $voitures = ModelVoiture::getVoitures(); //appel au modèle pour gerer la BD

        //require ('../view/voiture/list.php');  //"redirige" vers la vue
        ControllerVoiture::afficheVue("view.php", ["voitures" =>$voitures,"pagetitle" => "Liste des voitures SHESH", "cheminVueBody" => "voiture/list.php"]);
    }

    public static function read(int $imma) : void {
    $voiture = ModelVoiture::getVoitureParImmat($imma);
    if($voiture!=null) {
      //  require('../view/voiture/detail.php');
        ControllerVoiture::afficheVue("view.php",["voiture"=>$voiture,"pagetitle" => "Detail de la voiture " . $voiture->getImmatriculation(), "cheminVueBody" => "voiture/detail.php"]);
    }else{
       // require ('../view/voiture/error.php');
        ControllerVoiture::afficheVue("view.php",["pagetitle" => "ERROOOR", "cheminVueBody" => "voiture/error.php"]);
    }
    }

    public static function create(): void {
        ControllerVoiture::afficheVue("view.php",["pagetitle" => "Création", "cheminVueBody" => "voiture/create.php"]);
    }

    public static function created() :void {
        $marque=htmlspecialchars($_GET['marque']);
        $couleur=htmlspecialchars($_GET['couleur']);
        $immatriculation=htmlspecialchars($_GET['immatriculation']);
        $nbSieges=htmlspecialchars($_GET['nbSieges']);
        $voiture= new ModelVoiture($marque,$couleur,$immatriculation,$nbSieges);
        $voiture->sauvegarder();
        $voitures = ModelVoiture::getVoitures();
        ControllerVoiture::afficheVue("view.php",["pagetitle"=> "voiture crée","cheminVueBody"=>"voiture/created.php","voitures"=>$voitures]);
    }

    private static function afficheVue(string $cheminVue, array $parametres = []) : void {
        extract($parametres); // Crée des variables à partir du tableau $parametres
        // require "../view/$cheminVue"; // Charge la vue
        require_once __DIR__ . "/../view/$cheminVue";
    }



}
?>