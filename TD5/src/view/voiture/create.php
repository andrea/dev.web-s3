<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title> Formulaire voiture </title>
</head>

<body>

<form method="get" action="frontController.php">
    <fieldset>

        <legend>Mon formulaire :</legend>
        <input type="hidden" value="created" name="action"/>
        <p>
            <label for="immat_id">Immatriculation</label> :
            <input type="text" placeholder="256AB34" name="immatriculation"
                   id="immat_id" required/>
        </p>
        <p>
            <label for="couleur_id">Couleur</label> :
            <input type="text" placeholder="Blanc" name="couleur"
                   id="couleur_id" required/>
        </p>
        <p>
            <label for="marque_id">Marque</label> :
            <input type="text" placeholder="Fiat" name="marque"
                   id="marque_id" required/>
        </p>
        <p>
            <label for="sieges_id">Nombre de sièges</label> :
            <input type="number" placeholder="5" name="nbSieges"
                   id="sieges_id" required/>
        </p>
        <p>
            <input type="submit" value="Envoyer" />
        </p>
    </fieldset>
</form>
</body>
</html>