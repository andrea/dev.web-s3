<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title><?php echo $pagetitle; ?></title>
    <link rel="stylesheet" href="./css/style.css" />
</head>
<body>
<header>
    <nav id="menu">

      <a href="./frontController.php?action=readAll">Voitures</a>

                <a href="./frontController.php?action=readAll&controller=utilisateur" >Utilisateurs</a>
             <a href="./frontController.php?action=readAll&controller=trajet">Trajets</a>

    </nav>
</header>
<main>
    <?php
    require __DIR__ . "/{$cheminVueBody}";
    ?>
</main>
<footer>
    <p> Site de covoiturage de Anaïs baka</p>
</footer>
</body>
</html>