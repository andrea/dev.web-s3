<?php
//require_once 'ControllerVoiture.php';
require_once __DIR__. '/../src/Lib/Psr4AutoloaderClass.php';
use App\Covoiturage\Controller\ControllerVoiture;

// require_once __DIR__ . '/../src/Controller/ControllerVoiture.php';



// On recupère l'action passée dans l'URL
// URL examples:
//https://webinfo.iutmontp.univ-montp2.fr/~andrea/TD5/web/frontController.php?action=readAll

    // instantiate the loader
    $loader = new App\Covoiturage\Lib\Psr4AutoloaderClass();
    // register the base directories for the namespace prefix
    $loader->addNamespace('App\Covoiturage', __DIR__ . '/../src');
    // register the autoloader
    $loader->register();

    $action =$_GET['action'];
// Appel de la méthode statique $action de ControllerVoiture

    switch ($action){
        case "readAll": ControllerVoiture::$action();
        break;
        case "read": ControllerVoiture::$action($_GET["immatriculation"]);
        break;
        case "created": ControllerVoiture::$action();
        break;
        case "create": ControllerVoiture::$action();
    }

?>