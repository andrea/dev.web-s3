<?php
require_once ('../model/ModelVoiture.php'); // chargement du modèle

class ControllerVoiture {
    // Déclaration de type de retour void : la fonction ne retourne pas de valeur
    public static function readAll() : void {
        $voitures = ModelVoiture::getVoitures(); //appel au modèle pour gerer la BD

        //require ('../view/voiture/list.php');  //"redirige" vers la vue
        ControllerVoiture::afficheVue("voiture/list.php", ["voitures" =>$voitures]);
    }

    public static function read(int $imma) : void {
    $voiture = ModelVoiture::getVoitureParImmat($imma);
    if($voiture!=null) {
      //  require('../view/voiture/detail.php');
        ControllerVoiture::afficheVue("voiture/detail.php",["voiture"=>$voiture]);
    }else{
       // require ('../view/voiture/error.php');
        ControllerVoiture::afficheVue("voiture/error.php",[]);
    }
    }

    public static function create(): void {
        ControllerVoiture::afficheVue("voiture/create.php",[]);
    }

    public static function created() :void {
        $voiture= new ModelVoiture($_GET['marque'],$_GET['couleur'],$_GET['immatriculation'],$_GET['nbSieges']);
        $voiture->sauvegarder();
        self::readAll();
    }

    private static function afficheVue(string $cheminVue, array $parametres = []) : void {
        extract($parametres); // Crée des variables à partir du tableau $parametres
        require "../view/$cheminVue"; // Charge la vue
    }



}
?>