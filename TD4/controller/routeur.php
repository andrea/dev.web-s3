<?php
require_once 'ControllerVoiture.php';
// On recupère l'action passée dans l'URL
// URL examples:
// https://webinfo.iutmontp.univ-montp2.fr/~andrea/TD4/controller/routeur.php?action=readAll
// https://webinfo.iutmontp.univ-montp2.fr/~andrea/TD4/controller/routeur.php?action=read&immatriculation=12345678

    $action =$_GET['action'];
// Appel de la méthode statique $action de ControllerVoiture

    switch ($action){
        case "readAll": ControllerVoiture::$action();
        break;
        case "read": ControllerVoiture::$action($_GET["immatriculation"]);
        break;
        case "created": ControllerVoiture::$action();
        break;
        case "create": ControllerVoiture::$action();
    }

?>